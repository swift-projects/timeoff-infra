variable "project-id" {
  type    = string
  default = "terraform-gcp-349602"
}

variable "region" {
  type    = string
  default = "us-east1"
}

variable "zone" {
  type    = string
  default = "us-east1-a"
}

variable "network_name" {
  type    = string
  default = "timeoff-network"
}

variable "subnet_01" {
  type    = string
  default = "timeoff-subnet-01"
}

variable "subnet-01-gke-pods" {
  type    = string
  default = "subnet-01-gke-pods"
}

variable "subnet-01-gke-services" {
  type    = string
  default = "subnet-01-gke-services"
}

variable "repository-id" {
  type    = string
  default = "timeoff-repo"
}
