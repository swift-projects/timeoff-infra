provider "google" {
  credentials = file("credentials.json")
  project     = var.project-id
  region      = var.region
  zone        = var.zone
}

provider "google-beta" {
  credentials = file("credentials.json")
  project     = var.project-id
  region      = var.region
  zone        = var.zone
}

