terraform {
  backend "gcs" {
    bucket      = "terraform-gcp-godeveloper-net"
    prefix      = "timeoff-infra/state"
    credentials = "credentials.json"
  }
}

