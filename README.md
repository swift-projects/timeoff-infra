# Timeoff Management Application Infrastructure

![architecture](images/timeoff-infra.png)

## Introduction

In this project we are going to deploy the infrastructure necessary to deploy the application [Timeoff Management](https://github.com/timeoff-management/timeoff-management-application) using `Terraform` on Google Cloud Platform (GCP). To accomplish this task we need the following tools:

- Google Cloud Platform Account
- GitLab Account (not necessary)
- Terraform (1.0.0)

## Bootstrap

- Create a Google Cloud Project
- Create a Service Account
- Assign roles to Service Account
- Create Cloud Storage Bucket
- Enable Google APIs

### Create a Google Cloud Project

Create a project on your Google Cloud account and set the project Id in the `vars.tf` file.

```
variable "project-id" {
  type    = string
  default = "terraform-gcp-349602"
}
```

### Create Service Account

Create a Google Service Account and assign the following roles:

- Artifact Registry Administrator
- Cloud Run Admin
- Cloud SQL Admin
- Compute Network Admin
- Compute Security Admin
- DNS Administrator
- Secret Manager Admin
- Service Account User
- Storage Admin

### Add Key to Service Account

Go to (in order):

- IAM
- Service Accounts
- Identify the Service Account
- Actions
- Manage Keys
- Add Key
- Create new key
- JSON
- Create

Download the Google Cloud Service Account JSON file, rename the downloaded file to `credentials.json` and put it on the root of this project. (This file is registered on the Git ignore file).

### Create Cloud Storage Bukect

Create a Cloud Storage Bucket on your GCP project and set it in `state.tf` file.

```
terraform {
  backend "gcs" {
    bucket      = "terraform-gcp-godeveloper-net"
    prefix      = "timeoff-infra/state"
    credentials = "credentials.json"
  }
}
```

### Enable Google APIs

- Cloud Build API
- Secret Manager API
- Serverless VPC Access API

## Execute the project locally

We assume you have some basic experience with Google Cloud and Terraform, but if you follow the instructions on this document you should be good to go.

With everything in place you can run the following `terraform` commands:

```
terraform init
terraform plan
terraform apply (enter yes to confirm)
```

## GitLab Configuration

In this project there is a GitLab configuration file `gitlab-ci.yml`, needed by GitLab to run the pipeline every time there is a `push` in the `main` branch.

To configure the GitLab, go to the GitLab repository and set the following:

- In the project's respository go to `Settings`, `CI/CD`, in the `Variables` section hit the button `Expand`.
- Add the variables `GCP_ACCOUNT`, `GCP_PROJECT_ID`, `GCP_SERVICE_KEY` with their corresponding values.

`GCP_ACCOUNT` corresponds to the Google Service Account, something like `terraform-gcp@terraform-gcp-349602.iam.gserviceaccount.com`.
`GCP_SERVICE_KEY` corresponds to the content of the `credentials.json` file. The one you create while you're creating the Google Service Account.

Once you configure your GitLab project, you should be able to push changes to `main` branch and GitLab should run the `Terraform` code.

The `GitLab` configuration has the following steps:

- GCP Authentication
- Validate Terraform
- Production Plan
- Production Apply
- Production Destroy

From the step `GCP Authentication` to `Production Apply` are run automatically by `GitLab`, `Production Destroy` needs to be executed manually by a user. I set this configuration by convenience.

## Application Deployment

The application has its own repository and depends on this repo and the Google Cloud Infrastructure this project deploys to GCP.

You can follow the instructions in the following GitHub repository:

[Timeoff Management Application GitHub Repository](https://github.com/williammunozr/timeoff-management-application)

## References

- https://cloud.google.com/build/docs/securing-builds/use-secrets
- https://www.youtube.com/watch?v=6dLHcnlPi_U
