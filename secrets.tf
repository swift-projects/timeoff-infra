# Secrets

resource "random_string" "default" {
  length    = 7
  min_lower = 7
  lower     = true
  special   = false
}

resource "google_secret_manager_secret" "default" {
  secret_id = "DB_NAME"

  labels = {
    "terraformed" = "true"
  }

  replication {
    user_managed {
      replicas {
        location = "us-central1"
      }
      replicas {
        location = "us-east1"
      }
    }
  }
}

resource "google_secret_manager_secret_version" "default" {
  secret      = google_secret_manager_secret.default.id
  secret_data = random_string.default.result
}

resource "google_secret_manager_secret" "db_endpoint" {
  secret_id = "DB_ENDPOINT"

  labels = {
    "terraformed" = "true"
  }

  replication {
    user_managed {
      replicas {
        location = "us-central1"
      }
      replicas {
        location = "us-east1"
      }
    }
  }
}

resource "google_secret_manager_secret_version" "db_endpoint" {
  secret      = google_secret_manager_secret.db_endpoint.id
  secret_data = google_sql_database_instance.instance.private_ip_address
}

data "google_secret_manager_secret_version" "db_username" {
  secret = "DB_USERNAME"
}

data "google_secret_manager_secret_version" "db_password" {
  secret = "DB_PASSWORD"
}

data "google_secret_manager_secret_version" "db_dialect" {
  secret = "DB_DIALECT"
}

