resource "google_sql_database_instance" "instance" {
  name                = "timeoff-${google_secret_manager_secret_version.default.secret_data}"
  region              = var.region
  deletion_protection = false
  database_version    = "MYSQL_8_0"

  depends_on = [google_service_networking_connection.private_vpc_connection]

  settings {
    tier              = "db-f1-micro"
    availability_type = "ZONAL"
    disk_size         = 20

    ip_configuration {
      ipv4_enabled    = true
      private_network = module.vpc.network.network.id
    }
  }
}

resource "google_sql_database" "main" {
  name     = "main"
  instance = google_sql_database_instance.instance.name
}

resource "google_sql_user" "user" {
  name     = data.google_secret_manager_secret_version.db_username.secret_data
  instance = google_sql_database_instance.instance.name
  password = data.google_secret_manager_secret_version.db_password.secret_data
}

