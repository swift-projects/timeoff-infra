# Terraform Network Module
module "vpc" {
  source  = "terraform-google-modules/network/google"
  version = "3.3.0"

  project_id              = var.project-id
  network_name            = var.network_name
  auto_create_subnetworks = "false"
  routing_mode            = "REGIONAL"
  description             = "GKE VPC Network"

  subnets = [
    {
      subnet_name   = var.subnet_01
      subnet_ip     = "10.10.0.0/16"
      subnet_region = var.region
      description   = "GKE Subnet 01"
    }
  ]

  secondary_ranges = {
    gke-subnet-01 = [
      {
        range_name    = var.subnet-01-gke-pods
        ip_cidr_range = "192.168.64.0/20"
      },
      {
        range_name    = var.subnet-01-gke-services
        ip_cidr_range = "192.168.80.0/20"
      },
    ]
  }

  routes = [
    {
      name              = "egress-internet"
      description       = "route through IGW to access internet"
      destination_range = "0.0.0.0/0"
      tags              = "egress-inet"
      next_hop_internet = "true"
    }
  ]
}

resource "google_compute_global_address" "private_ip_address" {
  name          = "private-ip-address"
  purpose       = "VPC_PEERING"
  address_type  = "INTERNAL"
  prefix_length = 16
  ip_version    = "IPV4"
  network       = module.vpc.network.network.id
}

resource "google_service_networking_connection" "private_vpc_connection" {
  network                 = module.vpc.network.network.id
  service                 = "servicenetworking.googleapis.com"
  reserved_peering_ranges = [google_compute_global_address.private_ip_address.name]
}

resource "google_compute_firewall" "mysql" {
  name      = "allow-mysql"
  network   = module.vpc.network_name
  direction = "INGRESS"

  allow {
    protocol = "tcp"
    ports    = ["3306"]
  }

  source_ranges = ["0.0.0.0/0"] // ToDo: Allow only internal access
}

resource "google_vpc_access_connector" "connector" {
  name          = "timeoff-connector"
  provider      = google-beta
  region        = var.region
  ip_cidr_range = "10.8.0.0/28"
  network       = module.vpc.network_name
}
