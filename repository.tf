resource "google_artifact_registry_repository" "default" {
  provider      = google-beta
  location      = var.region
  repository_id = "timeoff-repo"
  description   = "Timeoff Repository"
  format        = "DOCKER"

  labels = {
    "terraformed" = "true"
    "project"     = "timeoff"
  }
}
